require('dotenv/config');
const Sequelize = require('sequelize')
const sequelize = new Sequelize(
  process.env.DB_NAME, //nome bd
  process.env.DB_USER, //usuário
  process.env.DB_PWD, { //senha
  host: process.env.DB_HOST,
  dialect: 'mysql'
});
 
/* const testCon = async () => {
  try {
    await sequelize.authenticate()
    console.log('conectado com sucesso')
  } catch (error) {
    console.log('conexao com bd falhou', error)
  }
}

testCon() */

module.exports = {
  Sequelize: Sequelize,
  sequelize: sequelize
}